/*
 * Question 4 (12 marks) Design Patterns

 * The Java class FileWriter is used for writing data to a file. 
 * It is desired to define a Java class for writing data to a 
 * file in an encrypted form. Show how to use the Decorator 
 * design pattern to define a class EncryptedFileWriter that
 * stores an encryption key and whose write methods call an encrypt
 * method to write an encrypted data to the decorated FileWriter.
 * 
 * An example of the intended usage is as follows.
 *		FileWriter out = new FileWriter(...);
 *		EncryptedFileWriter encryptout = new EncryptedFileWriter(out, key); 
 *		int x = ... ;
 *		encryptout.write(x);
 * EncryptedFileWriter encryptout = new EncryptedFileWriter(new FileWriter(), key);
 * Hints & Tips: Make sure that your design actually conforms to the Decorator pattern.
 */

// ASSUME FileWriter IMPLEMENTS IFileWriter
public class EncryptedFileWriter implements IFileWriter {
	private long key;
	private IFileWriter fw;

	public EncryptedFileWriter(IFileWriter fw, int key) {
		this.key = key;
		this.fw = fw;
	}

	public void write(int x) {
		// Here is the real 'decoration' - before writing to FileWriter, we encrypt the data
		String encrypted = encrypt(x);
		// Now we call the FileWriter write() function with our encrypted data
		fw.write(encrypted);
	}

	public void encrypt(int x) {
		// encrypt your stuff	
	}
}


/* Insert a UML diagram to demonstrate design ... Is that worth something I'm not sure. */
/* 12 marks what the fuck do I do !? */