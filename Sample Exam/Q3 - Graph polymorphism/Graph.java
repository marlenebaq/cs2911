import java.util.Collection;

/*
 * Question 3 (12 marks) Polymorphism
 * Define a generic Graph<E> interface type
 * for symmetric graphs that can handle elements
 * of a generic type E. Consider basic operations
 * for modifying graphs, and define an equals method.
 * Define a class invariant and show that your
 * implementation enforces this class invariant.
 * Hints & Tips: Argue that the class invariant
 * is maintained by referring directly to your code.
 */
public interface Graph<E> {
	public void addNode(E e);
	public void removeNode(E e);
	public void addEdge(E e1, E e2);
	public void removeEdge(E e1, E e2);
	public Collection<E> getNeighbours(E e);
}
