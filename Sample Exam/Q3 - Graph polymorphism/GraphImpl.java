import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;


// It is assumed that this is 1) a symmetric graph (as in the question) AND 2) not a weighted graph
/*
// Class invariant: edges.get(a).contains(b) == edges.get(b).contains(a)
 */
public class GraphImpl<E> implements Graph<E> {

	private HashMap<E, ArrayList<E>> adjMap;
	
	public GraphImpl() {
		adjMap = new HashMap<E, ArrayList<E>>();
		
	}
	
	@Override
	/*@
	@requires node does not already exist in graph
	 */
	public void addNode(E e) {
		adjMap.put(e, new ArrayList<E>());
	}

	@Override
	public void removeNode(E e) {
		adjMap.remove(e);
	}

	@Override
	public void addEdge(E e1, E e2) {
		adjMap.get(e1).add(e2);
		adjMap.get(e2).add(e1);
	}

	@Override
	public void removeEdge(E e1, E e2) {
		adjMap.get(e1).remove(e2);
		adjMap.get(e2).remove(e1);
	}

	@Override
	public Collection<E> getNeighbours(E e) {
		return (Collection<E>) adjMap.get(e);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
//		Can compare to other implementations of graph
		if (o instanceof Graph) {
			Graph g = (Graph) o;
//			Iterates through adjacency lists for each node (E) and checks if they have the same elements (order doesn't matter)
			for (E e : adjMap.keySet()) {
//				Can compare to other types of collections also
				Collection<E> oursAdj = adjMap.get(e);
				Collection<E> theirsAdj = g.getNeighbours(e);
				if (!oursAdj.containsAll(theirsAdj)) {
					return false;
				}
				if (!theirsAdj.containsAll(oursAdj)) {
					return false;
				}
			}
		}
		return true;
	}

}
