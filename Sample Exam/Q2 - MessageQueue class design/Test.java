
public class Test {
	public static void main(String[] args) {
		Test tester = new Test();
		tester.doTest1();
	}
	
	public Test() { }
	
	public boolean doTest1() {
		System.out.println("Doing test 1");
		MessageQueue queue = new MessageQueue(3);
		queue.add(new Message("Message 1"));
		queue.printState();
		queue.add(new Message("Message 2"));
		queue.printState();
		queue.add(new Message("Message 3"));
		queue.printState();
		queue.add(new Message("Message 4"));
		queue.printState();
		queue.add(new Message("Message 5"));
		queue.printState();
		System.out.print("Queue: " + queue.remove().getContents());
		System.out.print(", " + queue.remove().getContents());
		System.out.println(", " + queue.remove().getContents());
		queue.add(new Message("Message 6"));
		queue.add(new Message("Message 7"));
		queue.add(new Message("Message 8"));
		queue.add(new Message("Message 9"));
		queue.add(new Message("Message 10"));
		System.out.print("Queue: " + queue.remove().getContents());
		System.out.print(", " + queue.remove().getContents());
		System.out.println(", " + queue.remove().getContents());
		return true;
	}
}
