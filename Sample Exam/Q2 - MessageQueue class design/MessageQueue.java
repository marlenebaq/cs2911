/**
 * Question 2 (12 marks) Class Design
 * Design a MessageQueue class for implementing queues using a fixed size 
 * wrap-around array of Message elements. Implement methods for adding and 
 * removing elements from the queue, and give preconditions and postconditions
 * for each method in the class.
 * @author baquiranm
 *
 */

// assuming we are not meant to 'implement Queue<E>' ...
// this is almost like a C style question ?? Am I doing this right? 
// What else is involved in 'class design'

public class MaxCapacityException extends Exception {
  public MaxCapacityException(String message)
  {
    super(message);
  }
}

// BETTER PRECONS/POSTCONS DAMNIT!!!
public class MessageQueue {
	private Message[] array;
	private int head;
	private int tail;
	private int size;
	private int capacity;
	
	public MessageQueue(int capacity) {
		this.capacity = capacity;
		array = new Message[capacity];
		size = 0;
		head = 0;
		tail = 0;
	}
	
 //    @requires message parameter is a valid Message, size < capacity
 //    @ensures message element is added to queue - if
	//  * 			queue capacity is reached, wrap-around; queue size, 
	//  * 			tail and head are updated accordingly
 //     */
	public void add(Message message) {
		if (size == capacity) {
			throw new MaxCapacityException("Maximum number of elements in queue, cannot add any more");
		}
		size++;
		array[tail] = message;
		tail = (tail+1)%capacity;
	}
	
	// /*@
 //    @requires message queue is not empty
 //    @ensures earliest non-overwritten message is removed from the queue and returned
 //    @*/
	public Message remove() {
		Message message = array[head];
		array[head] = null;
		head = (head+1)%(capacity);
		size--;
		return message;
	}

	// stupid code for a queue that overwrites if at capacity. that's a dumb idea. design the class to guard against that instead. duh.
	// /*@
 //    @requires message parameter is a valid Message 
 //    @ensures message element is added to queue - if
	//  * 			queue capacity is reached, wrap-around; queue size, 
	//  * 			tail and head are updated accordingly
 //     */
	// public void add(Message message) {
	// 	if (size < capacity) {
	// 		size++;
	// 	}
	// 	array[tail] = message;
	// 	tail = (tail+1)%capacity;
	// 	if (size == capacity) {
	// 		head = (tail%capacity);
	// 	}
	// }
	
	// /*@
 //    @requires message queue is not empty
 //    @ensures earliest non-overwritten message is removed from the queue and returned
 //    @*/
	// public Message remove() {
	// 	Message message = array[head];
	// 	array[head] = null;
	// 	head = (head+1)%(capacity);
	// 	size--;
	// 	return message;
	// }
	
	public void printState() {
		System.out.println("Size: " + size + " Head: " + head + " Tail: " + tail);
	}
	
	public Message get(int index) {
		return array[index];
	}
	
	public int getSize() { return size; }

}
