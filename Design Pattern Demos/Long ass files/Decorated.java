// a decorator demo

public class Tester {
	public static void main(String[] args) {
		Tester t = new Tester();
		t.runTest();
	}

	public void runTest() {
		// Dessert dessert = new IceCream(); // note: implicit upcast here.
		Dessert fudgeIceCream = new DessertWithFudge(new IceCream());
		fudgeIceCream.prepare();
		System.out.println();
		Dessert fudgeCake = new DessertWithFudge(new DessertWithFruit(new Cake()));
		fudgeCake.prepare();
	}

	public Tester() { }
}

/*
 * Overarching interface
 */
public interface Dessert {
	public void prepare();
}

// Subject of decoration
public class Cake implements Dessert {
	public Cake() { }

	@Override
	public void prepare() {
		System.out.print("I've got a cake");
	}
}

// Subject of decoration
public class IceCream implements Dessert {
	public IceCream() { }

	@Override
	public void prepare() {
		System.out.print("I've got a cone of ice cream");
	}
}

// Decorator abstract class
public abstract class DessertTopper implements Dessert {
	protected Dessert dessert;

	public DessertTopper(Dessert dessert) {
		this.dessert = dessert;
	}

	@Override
	public void prepare() {
		this.dessert.prepare();
		System.out.print(" with ");
	}
}

// Concrete decorator
public class DessertWithFudge extends DessertTopper {
	public DessertWithFudge(Dessert dessert) {
		super(dessert);
	}

	@Override
	public void prepare() {
		super.prepare();
		System.out.print("fudge");
	}
}

public class DessertWithFruit extends DessertTopper {
	public DessertWithFruit(Dessert dessert) {
		super(dessert);
	}

	@Override
	public void prepare() {
		super.prepare();
		System.out.println("fruit");
	}
}