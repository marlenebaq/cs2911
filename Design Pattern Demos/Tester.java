import Composite.File;
import Composite.FileSystemResource;
import Composite.Folder;
import Decorator.Cake;
import Decorator.Dessert;
import Decorator.DessertWithFruit;
import Decorator.DessertWithFudge;
import Decorator.DessertWithSyrup;
import Decorator.IceCream;
import Iterator.Book;
import Iterator.BookCollection;
import Iterator.BookIterator;

/*
 * Design Patterns
In a manufacturing plant, a product (such as a car or a computer) is assembled from other parts
Manufactured parts, in turn, are assembled from smaller parts, which may themselves be basic or assembled parts
For example, a car might be assembled from a chassis, an engine and a body; in turn, the chassis might be assembled from a frame and some wheels, etc. (the details do not matter for the purposes of this exercise)
Use the Composite pattern to write Java classes for an Assembly and an Item with methods for calculating the total price of any part
Each Item has a given price, and the price of an Assembly is just the total price of all the parts in the assembly
Draw a UML class diagram for your program, making sure your code conforms to the Composite pattern
Use the Decorator pattern to allow discounted prices: discounts can apply to both basic and assembled parts, even already discounted parts
Extend the UML class diagram for your program, making sure your code conforms to both the Composite and Decorator patterns
 */
public class Tester {
	public static void main(String[] args) {
		Tester t = new Tester();
		t.testIterator();
		t.testComposite();
		t.testDecorator();
	}
	
	public void testIterator() {
		BookCollection collection = new BookCollection();
		collection.addBook(new Book("Batman", "Batman destroys millions of dollars worth of infrastructure"));
		collection.addBook(new Book("Fear and Trembling", "Here is a description"));
		collection.addBook(new Book("Book title", "Description about shit"));
		BookIterator it = (BookIterator) collection.createIterator();
		Book book1 = (Book) it.next();
		Book book2 = (Book) it.next();
		Book book3 = (Book) it.next();
		System.out.println(book1.getTitle() + ", " + book1.getDescription());
		System.out.println(book2.getTitle() + ", " + book2.getDescription());
		System.out.println(book3.getTitle() + ", " + book3.getDescription());
		book3 = (Book) it.prev();
		book2 = (Book) it.prev();
		book1 = (Book) it.prev();
		System.out.println(book3.getTitle() + ", " + book3.getDescription());
		System.out.println(book2.getTitle() + ", " + book2.getDescription());
		System.out.println(book1.getTitle() + ", " + book1.getDescription());
	}

	public void testDecorator() {
		// Dessert dessert = new IceCream(); // note: implicit upcast here.
		Dessert fudgeIceCream = new DessertWithFudge(new IceCream());
		fudgeIceCream.prepare();
		System.out.println();
		Dessert cake1 = new DessertWithFudge(new Cake());
		cake1.prepare();
		System.out.println();
		Dessert cake2 = new DessertWithFudge(new DessertWithSyrup(new DessertWithFruit(new Cake())));
		cake2.prepare();
	}
	
	public void testComposite() {
		Folder folder = new Folder();
		FileSystemResource f1 = new Folder(); // empty folder
		f1.nameResource("Folder 1");
		FileSystemResource f2 = new File();
		FileSystemResource f3 = new File();
		f3.nameResource("File 2");
		folder.addResource(f1);
		folder.addResource(f2);
		folder.addResource(f3);
		
		FileSystemResource f6 = new Folder();
		f6.nameResource("Things describing Matt Shao");
		FileSystemResource f4 = new File();
		f4.nameResource("Smelly");
		FileSystemResource f5 = new File();
		f5.nameResource("Gross");
		FileSystemResource f8 = new File();
		f8.nameResource("Chinese");
		FileSystemResource f7 = new File();
		f7.nameResource("Has butt where face should normally be");
		
		((Folder)f6).addResource(f5);
		((Folder)f6).addResource(f4);
		((Folder)f6).addResource(f7);
		((Folder)f6).addResource(f8);
		
		folder.addResource(f6);
		folder.nameResource("Top-level folder");
		folder.deleteResource();
	}

	public Tester() { }
}