package Iterator;
public interface Iterable {
	public IIterator createIterator();
}
