package Iterator;
import java.util.ArrayList;
import java.util.List;

public class BookCollection implements Iterable {

	private List<Book> books;
	
	public BookCollection() {
		books = new ArrayList<>();
	}
	
	public void addBook(Book b) {
		books.add(b);
	}
	
	@Override
	public IIterator createIterator() {
		return new BookIterator(this.books);
	}


}