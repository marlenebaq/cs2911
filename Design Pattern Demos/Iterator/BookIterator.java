package Iterator;
import java.util.List;

public class BookIterator implements IIterator {
	
	private int currentIndex;
	private List<Book> books;
	
	public BookIterator(List<Book> books) {
		this.books = books;
		currentIndex = 0;
	}

	@Override
	public boolean hasNext() {
		return currentIndex < books.size();
	}

	@Override
	public Object next() {
		if (hasNext()) return books.get(currentIndex++);
		return null;
	}

	@Override
	public Object prev() {
		if (hasPrev()) return books.get(--currentIndex);
		return null;
	}

	@Override
	public void remove() {
		books.remove(--currentIndex);
	}

	@Override
	public boolean hasPrev() {
		return currentIndex > 0;
	}

}
