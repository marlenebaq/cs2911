package Iterator;

public interface IIterator {
	public boolean hasNext();
	public boolean hasPrev();
	public Object next();
	public Object prev();
	public void remove();
}
