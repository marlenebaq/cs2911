package Composite;
public class File implements FileSystemResource {
	private String name;
	
	public File() {
		name = "New file";
	}

	@Override
	public void deleteResource() {
		System.out.println("Deleting a file named " + name);
	}

	@Override
	public void nameResource(String name) {
		this.name = name;
		System.out.println("Naming a file " + name);
	}
	
	@Override
	public String getName() { return name; }
}
