package Composite;
public interface FileSystemResource {
	public void deleteResource();
	public void nameResource(String name);
	public String getName();
}
