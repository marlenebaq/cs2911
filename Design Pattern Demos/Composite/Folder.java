package Composite;
import java.util.ArrayList;

public class Folder implements FileSystemResource {
	private String name;
	private ArrayList<FileSystemResource> subComponents;

	public Folder() {
		name = "New folder";
		subComponents = new ArrayList<FileSystemResource>();
	}

	@Override
	public void deleteResource() {
		for (FileSystemResource comp : subComponents) {
			comp.deleteResource();
		}
		System.out.println("Deleting a folder named " + name + " (we did it recursively !!! :))");
	}

	@Override
	public void nameResource(String name) {
		this.name = name;
		System.out.println("Naming a folder " + name);
	}
	
	@Override
	public String getName() { return name; }

	public void addResource(FileSystemResource f) {
		System.out.println("Adding " + f.getName() + " to " + this.name);
		subComponents.add(f);
	}
}