package Decorator;
public class DessertWithSyrup extends DessertTopper {
	public DessertWithSyrup(Dessert dessert) {
		super(dessert);
	}

	@Override
	public void prepare() {
		super.prepare();
		System.out.print("syrup");
	}
}