package Decorator;
public class DessertWithFudge extends DessertTopper {
	public DessertWithFudge(Dessert dessert) {
		super(dessert);
	}

	@Override
	public void prepare() {
		super.prepare();
		System.out.print("fudge");
	}
}