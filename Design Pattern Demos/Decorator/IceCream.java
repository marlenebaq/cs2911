package Decorator;
public class IceCream implements Dessert {
	public IceCream() { }

	@Override
	public void prepare() {
		System.out.print("I've got a cone of ice cream");
	}
}