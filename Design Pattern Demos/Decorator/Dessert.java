package Decorator;
public interface Dessert {
	public void prepare();
}