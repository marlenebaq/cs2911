package Decorator;
public class Cake implements Dessert {
	public Cake() { }

	@Override
	public void prepare() {
		System.out.print("I've got a cake");
	}
}