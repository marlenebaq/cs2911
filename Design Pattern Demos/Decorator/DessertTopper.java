package Decorator;
public abstract class DessertTopper implements Dessert {
	protected Dessert dessert;

	public DessertTopper(Dessert dessert) {
		this.dessert = dessert;
	}

	@Override
	public void prepare() {
		this.dessert.prepare();
		System.out.print(" with ");
	}
}