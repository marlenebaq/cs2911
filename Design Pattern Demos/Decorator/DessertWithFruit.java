package Decorator;
public class DessertWithFruit extends DessertTopper {
	public DessertWithFruit(Dessert dessert) {
		super(dessert);
	}

	@Override
	public void prepare() {
		super.prepare();
		System.out.print("fruit");
	}
}