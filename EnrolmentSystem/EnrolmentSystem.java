import java.util.ArrayList;
import java.util.HashMap;

// time for shite working implementation: 45 minutes

public class EnrolmentSystem {
	private ArrayList<Student> allStudents;
	private ArrayList<Course> allCourses;
	
	public EnrolmentSystem() {
		allStudents = new ArrayList<Student>();
		allCourses = new ArrayList<Course>();
	}
	
	public static void main(String[] args) {
		EnrolmentSystem system = new EnrolmentSystem();
		
		system.test1();
		system.test2();
	}
	
	private boolean enrolStudent(Student student, Course course) {
		ArrayList<String> studentPasses = student.getPassedCourses();
		if (course.prereqsFulfilled(studentPasses)) {
			System.out.println(student.getGivenName() + " passed all prereqs for " + course.getCode());
			CourseEnrolment enrolment = course.makeEnrolment();
			if (enrolment == null) {
				System.out.println("No more sessions available for " + student.getGivenName() + "! :(");
				return false;
			} else {
				student.addEnrolment(enrolment);
				return true;
			}
		}
		System.out.println(student.getGivenName() + " has not passed all prereqs for " + course.getCode());
		return false;
	}
	
//	pre-condition
	private void addStudent(String surname, String givenName) {
		allStudents.add(new Student(surname, givenName, allStudents.size()));
	}
	
	private Student findStudent(String surname, String givenName) {
		for (Student s : allStudents) {
			if (s.getSurname().equals(surname) && s.getGivenName().equals(givenName)) {
				return s;
			}
		}
		return null;
	}
	
	private void test1() {
		ArrayList<String> prereq1 = new ArrayList<String>();
		prereq1.add("COMP1917");
		prereq1.add("COMP1927");
		ArrayList<Session> availSessions1 = new ArrayList<Session>();
		Session lc1 = new Session("LECTURE", "Monday", "13:00-15:00");
		Session lb1 = new Session("LAB", "Thursday", "12:00-14:00");
		Session tu1 = new Session("TUTORIAL", "Wednesday", "18:00-20:00");
		Session lc2 = new Session("LECTURE", "Monday", "11:00-13:00");
		Session lb2 = new Session("LAB", "Thursday", "10:00-12:00");
		Session tu2 = new Session("TUTORIAL", "Wednesday", "13:00-15:00");
		availSessions1.add(lc1);
		availSessions1.add(lb1);
		availSessions1.add(tu1);
		availSessions1.add(lc2);
		availSessions1.add(lb2);
		availSessions1.add(tu2);
		Course COMP2911 = new Course("COMP2911", 6, prereq1, availSessions1);
		allCourses.add(COMP2911);
		
		addStudent("Baquiran", "Marlene");
		Student s1 = findStudent("Baquiran", "Marlene");
		HashMap<String, Grade> pastCourseResults1 = new HashMap<String, Grade>();
		pastCourseResults1.put("COMP1917", Grade.P);
		pastCourseResults1.put("COMP1927", Grade.DN);
		pastCourseResults1.put("MATH1131", Grade.DN);
		s1.setPastCourseResults(pastCourseResults1);
		
		addStudent("Shao", "Matt");
		Student s2 = findStudent("Shao", "Matt");
		HashMap<String, Grade> pastCourseResults2 = new HashMap<String, Grade>();
		pastCourseResults2.put("COMP1917", Grade.DN);
		pastCourseResults2.put("COMP1927", Grade.HD);
		pastCourseResults2.put("FINS2121", Grade.DN);
		s2.setPastCourseResults(pastCourseResults2);
		
		enrolStudent(s1, COMP2911);
		enrolStudent(s2, COMP2911);
		
		for (Student s : allStudents) {
			System.out.println(s.toString());
		}
	}
	
	private void test2() {
		ArrayList<String> prereq1 = new ArrayList<String>();
		prereq1.add("COMP1917");
		prereq1.add("COMP1927");
		ArrayList<Session> availSessions1 = new ArrayList<Session>();
		Session lc1 = new Session("LECTURE", "Monday", "13:00-15:00");
		Session lb1 = new Session("LAB", "Thursday", "12:00-14:00");
		Session tu1 = new Session("TUTORIAL", "Wednesday", "18:00-20:00");
		Session lc2 = new Session("LECTURE", "Tuesday", "14:00-16:00");
		Session lb2 = new Session("LAB", "Friday", "12:30-14:30");
		Session tu2 = new Session("TUTORIAL", "Wednesday", "18:00-20:00");
		availSessions1.add(lc1);
		availSessions1.add(lb1);
		availSessions1.add(tu1);
		availSessions1.add(lc2);
		availSessions1.add(lb2);
		availSessions1.add(tu2);
		Course COMP2911 = new Course("COMP2911", 6, prereq1, availSessions1);
		allCourses.add(COMP2911);
		
		addStudent("Aung", "Yaminn");
		Student s1 = findStudent("Aung", "Yaminn");
		HashMap<String, Grade> pastCourseResults1 = new HashMap<String, Grade>();
		pastCourseResults1.put("COMP1917", Grade.P);
		pastCourseResults1.put("COMP1927", Grade.DN);
		pastCourseResults1.put("MATH1131", Grade.DN);
		s1.setPastCourseResults(pastCourseResults1);
		
		addStudent("Song", "Suegin");
		Student s2 = findStudent("Song", "Suegin");
		HashMap<String, Grade> pastCourseResults2 = new HashMap<String, Grade>();
		pastCourseResults2.put("COMP1927", Grade.HD);
		pastCourseResults2.put("FINS2121", Grade.DN);
		pastCourseResults2.put("COMP1917", Grade.DN);
		s2.setPastCourseResults(pastCourseResults2);
		
		addStudent("Mitic", "Milena");
		Student s3 = findStudent("Mitic", "Milena");
		HashMap<String, Grade> pastCourseResults3 = new HashMap<String, Grade>();
		pastCourseResults3.put("COMP1927", Grade.HD);
		pastCourseResults3.put("FINS2121", Grade.DN);
		s3.setPastCourseResults(pastCourseResults3);
		
		addStudent("Cow", "Brown");
		Student s4 = findStudent("Cow", "Brown");
		HashMap<String, Grade> pastCourseResults4 = new HashMap<String, Grade>();
		pastCourseResults4.put("COMP1927", Grade.HD);
		pastCourseResults4.put("FINS2121", Grade.DN);
		pastCourseResults4.put("COMP1917", Grade.HD);
		s4.setPastCourseResults(pastCourseResults4);
		
		enrolStudent(s1, COMP2911);
		enrolStudent(s2, COMP2911);
		enrolStudent(s3, COMP2911);
		enrolStudent(s4, COMP2911);
		
		for (Student s : allStudents) {
			System.out.println(s.toString());
		}
	}
}
