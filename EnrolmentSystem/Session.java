
public class Session {
	private String type;
	private String day;
	// just a simple indicator of time
	private String time;
	
	public Session(String type, String day, String time) {
		this.type = type;
		this.day = day;
		this.time = time;
	}

	public String getDay() {
		return day;
	}

	public String getTime() {
		return time;
	}

	public String getType() {
		return type;
	}
	
	@Override
	public String toString() {
		return type + ": " + day + " " + time; 
	}
}
