import java.util.ArrayList;

public class Course {
	private String courseCode;
	private int creditPoints;
	private ArrayList<String> prereqCodes;
	// for simplicity we assume only 1 student per session for now ...
	private ArrayList<Session> availSessions;
	
	public Course(String courseCode, int creditPoints, ArrayList<String> prereqCodes, ArrayList<Session> availSessions) {
		this.courseCode = courseCode;
		this.creditPoints = creditPoints;
		this.prereqCodes = prereqCodes;
		this.availSessions = availSessions;
	}
	
	public String getCode() {
		return courseCode;
	}
	
	@Override
	public String toString() {
		String prereqs = "";
		for (String p : prereqCodes) {
			if (p.equals(prereqCodes.get(0))) {
				prereqs = "Prereqs: " + p;
			} else {
				prereqs = prereqs + ", " + p;
			}
		}
		return courseCode + " (" + creditPoints + "UOC) " + prereqs;
	}
	
	// right now gets first available slots
//	book one tute, lecture and lab
	// precondition: student has passed all prereqs
	
	public boolean prereqsFulfilled(ArrayList<String> prereqCodes) {
		return prereqCodes.containsAll(this.prereqCodes);
	}
	
	public CourseEnrolment makeEnrolment() {
		Session tute = null;
		Session lab = null;
		Session lecture = null;
		for (Session s : availSessions) {
			// for a more complex implementation, would probably make these subclasses of Session instead.
			if (s.getType().equals("TUTORIAL") && tute == null) {
				tute = s;
			} else if (s.getType().equals("LAB") && lab == null) {
				lab = s;
			} else if (s.getType().equals("LECTURE") && lecture == null) {
				lecture = s;
			}
		}
		
		if (tute == null || lecture == null || lab == null) {
			return null;
		}
		
		availSessions.remove(tute);
		availSessions.remove(lab);
		availSessions.remove(lecture);
		
		return new CourseEnrolment(this.courseCode, this.creditPoints, lecture, lab, tute);
	}
}
