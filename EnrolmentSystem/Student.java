import java.util.ArrayList;
import java.util.HashMap;

public class Student {
	String surname;
	String givenName;
	int id;
	ArrayList<CourseEnrolment> currentEnrolments;
	HashMap<String, Grade> pastCourseResults;
	
	public Student(String surname, String givenName, int id) {
		this.surname = surname;
		this.givenName = givenName;
		this.id = id;
		currentEnrolments = new ArrayList<CourseEnrolment>();
	}

	public void setPastCourseResults(HashMap<String, Grade> pastCourseResults) {
		this.pastCourseResults = pastCourseResults;
	}
	
	public ArrayList<String> getPassedCourses() {
		ArrayList<String> passedCourses = new ArrayList<String>();
		for (String c : pastCourseResults.keySet()) {
			if (!pastCourseResults.get(c).equals(Grade.F)) {
				passedCourses.add(c);
			}
		}
		return passedCourses;
	}
	
	public void addEnrolment(CourseEnrolment enrolment) {
		currentEnrolments.add(enrolment);
	}

	@Override
	public String toString() {
		return "==SID: " + id + ", " + givenName + " " + surname + "==\n" + pastCourseResults.toString() + "\n" + currentEnrolments.toString();
		
	}
	
	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ArrayList<CourseEnrolment> getCurrentEnrolments() {
		return currentEnrolments;
	}

	public void setCurrentEnrolments(ArrayList<CourseEnrolment> currentEnrolments) {
		this.currentEnrolments = currentEnrolments;
	}
}
