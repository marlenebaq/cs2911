
public class CourseEnrolment {
	private String courseCode;
	private int creditPoints;
	private Session lecture;
	private Session lab;
	private Session tutorial;
	
	public CourseEnrolment(String courseCode, int creditPoints, Session lecture, Session lab, Session tutorial) {
		this.courseCode = courseCode;
		this.creditPoints = creditPoints;
		this.lecture = lecture;
		this.lab = lab;
		this.tutorial = tutorial;
	}
	
	@Override
	public String toString() {
		return courseCode + " (" + creditPoints + "UOC) " + lecture.toString() + ", " + lab.toString() + ", " + tutorial.toString();
	}

	public Session getLecture() {
		return lecture;
	}

	public Session getLab() {
		return lab;
	}

	public Session getTutorial() {
		return tutorial;
	}
	
	
}
