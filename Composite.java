
// a composite demo
// 5 minutes for most basic implementation
import java.util.ArrayList;
import java.util.List;

/*
 * Design Patterns
In a manufacturing plant, a product (such as a car or a computer) is assembled from other parts
Manufactured parts, in turn, are assembled from smaller parts, which may themselves be basic or assembled parts
For example, a car might be assembled from a chassis, an engine and a body; in turn, the chassis might be assembled from a frame and some wheels, etc. (the details do not matter for the purposes of this exercise)
Use the Composite pattern to write Java classes for an Assembly and an Item with methods for calculating the total price of any part
Each Item has a given price, and the price of an Assembly is just the total price of all the parts in the assembly
Draw a UML class diagram for your program, making sure your code conforms to the Composite pattern
Use the Decorator pattern to allow discounted prices: discounts can apply to both basic and assembled parts, even already discounted parts
Extend the UML class diagram for your program, making sure your code conforms to both the Composite and Decorator patterns
 */
public class Tester {
	public static void main(String[] args) {
		Tester t = new Tester();
		t.testComposite();
	}

//	public void testDecorator() {
//		// Dessert dessert = new IceCream(); // note: implicit upcast here.
//		Dessert fudgeIceCream = new DessertWithFudge(new IceCream());
//		fudgeIceCream.prepare();
//		System.out.println();
//		Dessert fudgeCake = new DessertWithFudge(new DessertWithFruit(new Cake()));
//		fudgeCake.prepare();
//	}
	
	public void testComposite() {
		Folder folder = new Folder();
		FileSystemResource f1 = new Folder(); // empty folder
		f1.nameResource("Folder 1");
		FileSystemResource f2 = new File();
		FileSystemResource f3 = new File();
		f3.nameResource("File 2");
		folder.addResource(f1);
		folder.addResource(f2);
		folder.addResource(f3);
		
		FileSystemResource f6 = new Folder();
		f6.nameResource("Things describing Matt Shao");
		FileSystemResource f4 = new File();
		f4.nameResource("Smelly");
		FileSystemResource f5 = new File();
		f5.nameResource("Gross");
		FileSystemResource f8 = new File();
		f8.nameResource("Chinese");
		FileSystemResource f7 = new File();
		f7.nameResource("Has butt where face should normally be");
		
		((Folder)f6).addResource(f5);
		((Folder)f6).addResource(f4);
		((Folder)f6).addResource(f7);
		((Folder)f6).addResource(f8);
		
		folder.addResource(f6);
		folder.nameResource("Top-level folder");
		folder.deleteResource();
	}

	public Tester() { }
}


public interface FileSystemResource {
	public void deleteResource();
	public void nameResource(String name);
	public String getName();
}



public class File implements FileSystemResource {
	private String name;
	
	public File() {
		name = "New file";
	}

	@Override
	public void deleteResource() {
		System.out.println("Deleting a file named " + name);
	}

	@Override
	public void nameResource(String name) {
		this.name = name;
		System.out.println("Naming a file " + name);
	}
	
	@Override
	public String getName() { return name; }
}



public class Folder implements FileSystemResource {
	private String name;
	private ArrayList<FileSystemResource> subComponents;

	public Folder() {
		name = "New folder";
		subComponents = new ArrayList<FileSystemResource>();
	}

	@Override
	public void deleteResource() {
		for (FileSystemResource comp : subComponents) {
			comp.deleteResource();
		}
		System.out.println("Deleting a folder named " + name + " (we did it recursively !!! :))");
	}

	@Override
	public void nameResource(String name) {
		this.name = name;
		System.out.println("Naming a folder " + name);
	}
	
	@Override
	public String getName() { return name; }

	public void addResource(FileSystemResource f) {
		System.out.println("Adding " + f.getName() + " to " + this.name);
		subComponents.add(f);
	}
}