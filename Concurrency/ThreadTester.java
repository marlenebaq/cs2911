/**
   This program runs two threads in parallel.
*/

/**
 * Consider a wrap around queue used for producers and consumers
Explain why the implementation of the BoundedQueue class is not threadsafe
Use a re-entrant lock with two conditions to make the implementation threadsafe
The tests for whether the queue is not full/not empty should be moved to the add/remove methods
The add and remove methods should acquire the lock before executing, then release the lock when finished
When the queue is full, the add method should wait for space available, and signal value available before returning
When the queue is empty, the remove method should wait for value available, and signal space available before returning
 * @author baquiranm
 *
 */
public class ThreadTester
{
   public static void main(String[] args)
   {
      BoundedQueue<String> queue = new BoundedQueue<String>(10);
      queue.setDebug(false);
      final int GREETING_COUNT = 100;
      Runnable run1 = new Producer("Hello, World!", 
            queue, GREETING_COUNT);
      Runnable run2 = new Producer("Goodbye, World!", 
            queue, GREETING_COUNT);
      Runnable run3 = new Consumer(queue, 2 * GREETING_COUNT); // should remove all HelloWorlds/GoodbyeWorlds
      
      Thread thread1 = new Thread(run1);
      Thread thread2 = new Thread(run2);
      Thread thread3 = new Thread(run3);

      thread1.start();
      thread2.start();
      thread3.start();
   }
}

