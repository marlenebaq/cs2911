import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * An action that repeatedly removes a greeting from a queue.
 */
// only consume if not empty
public class Consumer implements Runnable {
	final Lock lock = new ReentrantLock();
	final Condition notfull = lock.newCondition();
	final Condition notempty = lock.newCondition();

	/**
	 * Constructs the consumer object.
	 * 
	 * @param aQueue
	 *            the queue from which to retrieve greetings
	 * @param count
	 *            the number of greetings to consume
	 */
	public Consumer(BoundedQueue<String> aQueue, int count) {
		queue = aQueue;
		greetingCount = count;
	}

	@Override
	public void run() {
		// lock.lock();
		try { // just pop off of the queue greetingCount number of times
			int i = 1;
			while (i <= greetingCount) {
				String greeting = queue.remove();
				System.out.println(greeting);
				i++;
				Thread.sleep((int) (Math.random() * DELAY));
			}
		} catch (InterruptedException exception) { }
	}

	private BoundedQueue<String> queue;
	private int greetingCount;

	private static final int DELAY = 10;
}
