import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * A first-in, first-out bounded collection of objects.
 */
public class BoundedQueue<E> {
	final Lock lock = new ReentrantLock(); // used to prevent other threads from accessing the method
	final Condition notFull = lock.newCondition(); // used to notify blocked processes that a condition
													// has been fulfilled, and that it may continue
	final Condition notEmpty = lock.newCondition();

	/**
	 * Constructs an empty queue.
	 * 
	 * @param capacity
	 *            the maximum capacity of the queue
	 */
	public BoundedQueue(int capacity) {
		elements = new Object[capacity];
		head = 0;
		tail = 0;
		size = 0;
	}

	/**
	 * Removes the object at the head.
	 * 
	 * @return the object that has been removed from the queue
	 * @throws InterruptedException 
	 * @precondition !isEmpty()
	 */
	public E remove() throws InterruptedException {
		lock.lock(); // ADDED THIS
		while (isEmpty()) {
			notEmpty.await();// ADDED THIS
		}
		E r = (E) elements[head];
		try { 
			notFull.signalAll();// ADDED THIS
			head++;
			size--;
			if (head == elements.length) {
				head = 0;
			}
		} finally {
			lock.unlock();// ADDED THIS - always executes
		}
		return r;
	}

	/**
	 * Appends an object at the tail.
	 * 
	 * @param newValue
	 *            the object to be appended
	 * @throws InterruptedException 
	 * @precondition !isFull();
	 */
	public void add(E newValue) throws InterruptedException {
		lock.lock();// ADDED THIS
		while(isFull()) {
			notFull.await();// ADDED THIS
		}
		try {
			elements[tail] = newValue;
			notEmpty.signal();// ADDED THIS
			tail++;
			size++;
			if (tail == elements.length) {
				tail = 0;
			}
//				System.out.println("head=" + head + ",tail=" + tail + ",size=" + size);
		} finally {
			lock.unlock();// ADDED THIS - always executes
		}
	}

	public boolean isFull() {
		return size == elements.length;
	}

	public boolean isEmpty() {
		return size == 0;
	}

	public void setDebug(boolean newValue) {
		debug = newValue;
	}

	private Object[] elements;
	private int head;
	private int tail;
	private int size;
	private boolean debug;
}
